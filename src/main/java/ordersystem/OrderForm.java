package ordersystem;
/*
    Copyright (C) 2019 James R. Young

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

/**
 * Represents an order form invoice for a customer's number, first name, last name, invoice lines,
 * and the entire order total.
 */
public class OrderForm {

	private long customerNumber;
	private String firstName;
	private String lastName;
	private Collection<InvoiceLine> lines = new ArrayList<>();
	private double orderTotal;

	public OrderForm(Customer customer, Map<Line, Product> lineToProduct) {
		customerNumber = customer.getNumber();
		firstName = customer.getFirstName();
		lastName = customer.getLastName();
		double orderTotal = 0;
		for (Line l : lineToProduct.keySet()) {
			Product product = lineToProduct.get(l);
			double lineTotal = l.getQuantity() * product.getUnitPrice();
			orderTotal += lineTotal;
			lines.add(new InvoiceLine(l, product.getDescription(), product.getUnitPrice(), lineTotal));
		}
		this.orderTotal = orderTotal;
	}

	/**
	 * Gets the customer number
	 * @return the customer number
	 */
	public long getCustomerNumber() {
		return customerNumber;
	}

	/**
	 * Gets the customer's first name
	 * @return the customer's first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Gets the customer's last name
	 * @return the customer's last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Gets all Invoice Lines
	 * @return all Invoice Lines
	 */
	public Collection<InvoiceLine> getLines() {
		return lines;
	}

	/**
	 * Gets the total cost of the order
	 * @return the total cost of the order
	 */
	public double getOrderTotal() {
		return orderTotal;
	}

}
