package ordersystem;
/*
    Copyright (C) 2019 James R. Young

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Adds extra items to a Line item: description, unit price, and line total
 */
public class InvoiceLine extends Line {

	private String description;
	private double unitPrice;
	private double lineTotal;

	public InvoiceLine(Line line, String description, double unitPrice, double lineTotal) {
		super(line.getSku(), line.getQuantity());
		this.description = description;
		this.unitPrice = unitPrice;
		this.lineTotal = lineTotal;
	}

	/**
	 * Gets the line total price
	 * @return the line total price
	 */
	public double getLineTotal() {
		return lineTotal;
	}

	/**
	 * Gets the description of the line item
	 * @return the description of the line item
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Gets the unit price of the line item
	 * @return the unit price of the line item
	 */
	public double getUnitPrice() {
		return unitPrice;
	}

}