package ordersystem;
/*
   Copyright (C) 2018 Karl R. Wurst
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
/**
 * Controller for REST API endpoints for Customers
 *
 * @author Karl R. Wurst
 * @version Fall 2018
 */
@RestController
public class CustomerController {
	private static final AtomicLong counter = Database.getCustomerCounter();
	private static Map<Long, Customer> customerDb = Database.getCustomerDb();

	/**
	 * Create a new customer in the database
	 * @param customer customer with first and last names
	 * @return the customer number
	 */
	@CrossOrigin() // to allow CORS requests when running as a local server
	@PostMapping("/customers/new")
	public ResponseEntity<Long> addNewCustomer(@RequestBody Customer customer) {
		customer.setNumber(counter.incrementAndGet());
		customerDb.put(customer.getNumber(), customer);
		return new ResponseEntity<>(customer.getNumber(), HttpStatus.CREATED);
	}

	/**
	 * Get a customer from the database
	 * @param number the customer number
	 * @return the customer if in the database, not found if not
	 */
	@GetMapping("/customers/{number}")
	public ResponseEntity<Object> getCustomerByNumber(@PathVariable long number) {
		if (customerDb.containsKey(number)) {
			return new ResponseEntity<>(customerDb.get(number), HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Customer does not exist", HttpStatus.NOT_FOUND);
		}
	}

	/**
	 * Get an array of all customers from the database
	 * @return array of all customers in the database, not found if empty
	 */
	@GetMapping("/customers")
	public ResponseEntity<Object> getCustomers() {
		Collection<Customer> customerCollection = customerDb.values();
		if (customerCollection.isEmpty()) {
			return new ResponseEntity<>("No customers found", HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<>(customerCollection, HttpStatus.OK);
		}
	}

	/**
	 * Update the name of a customer in the database
	 * @param number the customer number
	 * @param customer customer with first and last names
	 * @return successful if the customer is in the database, not found if not
	 */
	@PatchMapping("/customers/{number}")
	public ResponseEntity<Object> updateCustomerName(@PathVariable long number, @RequestBody Customer customer) {
		if (customerDb.containsKey(number)) {
			customerDb.get(number).setFirstName(customer.getFirstName());
			customerDb.get(number).setLastName(customer.getLastName());
			return new ResponseEntity<>("Update successful", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Customer does not exist", HttpStatus.NOT_FOUND);
		}
	}

	/**
	 * Gets all customer numbers with a given the first and last name
	 * @param firstName the customer's first name
	 * @param lastName the customer's last name
	 * @return array of customer numbers if the customer exists, not found if not
	 */
	@GetMapping("/customers/{firstName}/{lastName}")
	public ResponseEntity<Object> getCustomerNumberByName(@PathVariable String firstName, @PathVariable String lastName) {
		ArrayList<Long> customerNumbers = new ArrayList<>();
		for (Customer customer : customerDb.values()) {
			String customerFirst = customer.getFirstName().toLowerCase();
			String customerLast = customer.getLastName().toLowerCase();
			if (customerFirst.equals(firstName.toLowerCase()) && customerLast.equals(lastName.toLowerCase())) {
				customerNumbers.add(customer.getNumber());
			}
		}

		if (customerNumbers.size() > 0) {
			return new ResponseEntity<>(customerNumbers, HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Customer does not exist", HttpStatus.NOT_FOUND);
		}
	}
}