Start server with the command `./gradlew bootRun`

REST API endpoints:

Method | Endpoint | Body | Return | Note
--- | --- | --- | --- | ---
POST | /customers/new | {"firstName": "*firstName*", "lastName": "*lastName*" } | *customerNumber* | Adds new customer to database
GET | /customers/*number* | | {"number" : *number*, "firstName": "*firstName*", "lastName": "*lastName*"} | Returns customer with given number
POST | /products/new | {"price": *price*, "description": *description*} | *sku* | Adds new product to database
GET | /products/*sku* | | {"sku": *sku*, "price": *price*, "description": *description*} | Returns product with given number
POST | /orders/new/*customerNumber* | | *orderNumber* | Adds new order to database for this customer
GET | /orders/*number*/customer | | *customerNumber* | Returns customer for this order number
GET | /orders/*number*/lines | | [{"sku": *sku*, "quantity": *quantity*}, ...] | Returns array of order lines for this order
PUT | /orders/*number*/lines | {"sku": *sku*, "quantity": *quantity*} | | Increase quantity of the product in this order
DELETE | /orders/*number*/lines | {"sku": *sku*, "quantity": *quantity*} | | Decrease quantity of the product in this order
GET | /customers/ | | [{"firstName": *firstName*, "lastName": *lastName*}, ...] | Returns array of all customers
PATCH | /customers/{*customerNumber*} | {"number": "*number*", "firstName": "*firstName*", "lastName": "*lastName*" } | | Updates customer first name and last name with given number
GET | /customers/*firstName*/*lastName* | | [*customerNumber*, ...] | Returns array of all customer numbers for a given first and last name
GET | /orders/customer/*number* | | [*orderNumber*, ...] | Returns array of all order numbers for a given customer number
GET | /orders/*number*/invoice | | { "customerNumber": *number*, "firstName": *firstName*, "lastName": *lastName*, "lines":[ {"sku": *sku*, "quantity": *quantity*, "description": *description*, "unitPrice": *unitPrice*, "lineTotal": *lineTotal*} ...], "orderTotal": *orderTotal* } | Returns an order form invoice for a given order number
PATCH | /products | {"sku": *sku*, "unitPrice" : *price*} | | Update unit price given a product's sku
POST | /shutdown | | | Shuts down the server
